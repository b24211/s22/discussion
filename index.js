// Mutator Methods

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() - Adds an element in the end of an array AND returns the array's length

console.log('Current Array: ');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method: ');
console.log(fruits);

// Adding multiple elements in an array
fruits.push('Avacado', 'Gauva');
console.log('Mutated array from push method: ');
console.log(fruits);

// pop() - Removes the last element in an array and returns the removed element
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method: ')
console.log(fruits);

// unshift() - Adds one or more elements at the beginning of an array
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method: ')
console.log(fruits);

// shift() - Removes an element at the beginning of an array AND returns the removed element
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method: ');
console.log(fruits);

// splice() - Simultaneously removes elements from a specified index number and adds elements
/*
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method: ');
console.log(fruits);

// sort() - Rearranges the array elements in alphanumeric order 
fruits.sort();
console.log('Mutated array from sort method: ');
console.log(fruits);

// reverse() - Reverses the order of array elements
fruits.reverse();
console.log('Mutated array from reverse method: ')
console.log(fruits);

// Non-mutator methods
let countries = ['US', 'PH', 'IN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() - Return the index number of the first element found in an array
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf() - Returns the index number of the last matching element found in an array.

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of indexOf method: ' + lastIndex);

// Getting the index number starting from the specified element
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of indexOf method: ' + lastIndexStart);

// slice() - Portions/slices elements from an array AND returns a new array

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result of slice method: ');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index.
let slicedArrayB = countries.slice(2, 4);
console.log('Result of slice method: ');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log('Result of slice method: ');
console.log(slicedArrayC);

// toString() - Returns an array as a string seperated by commas
let stringArray = countries.toString();
console.log('Result of toString method: ');
console.log(stringArray);

// concat() - Combines two arrays and returns the combined result

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result of concat method: ');
console.log(tasks);

// Combining multiple arrays
console.log('Result of concat method: ');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// join() - Returns an array as a string seperated by specified seperated string

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods

// forEach() - Similar to a for loop that iterates on each array element 
/*
	Syntax:
		arrayName.forEach(function(indivElement) {statement})
*/

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements
let filteredTasks = [];
allTasks.forEach(function(task){
	console.log(task);

	// If the element/string's length is greater than 10 characters
	if(task.length > 10){
		console.log(task);
		filteredTasks.push(task);
	}
})
console.log("Result of filtered tasks: ");
console.log(filteredTasks);

// map() - Iterate on each element AND returns a new array with different values depending on the result of the function's operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number*number;
})
console.log("Original Array:")
console.log(numbers);
console.log("Result of map method");
console.log(numberMap);

console.log('Using forEach with operation:')
let numberForEach = numbers.forEach(function(number){
	return number*number;
})
console.log(numberForEach);

// every() - Checks if all elements in an array meet the given condition

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Result of every method:")
console.log(allValid);

// some() - Check if atleast one element in the array meets the given condition

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some method:")
console.log(someValid);

// filter() - Returns new array that contains elements which meets the given condition

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result of filter method:")
console.log(filterValid);

// includes() - Checks if the argument contains the list of the array
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log("Result of includes method:");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce() - Evaluates elements from left to right and returns/reduces the array into a single value

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// The operation to reduce the array into a single value
	return x + y;
})
console.log('Result of reduce method: ' + reducedArray);